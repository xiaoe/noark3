/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.orm.accessor.sql;

import xyz.noark.core.exception.UnrealizedException;
import xyz.noark.orm.DataConstant;
import xyz.noark.orm.EntityMapping;
import xyz.noark.orm.FieldMapping;

/**
 * SQL专家.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public abstract class AbstractSqlExpert implements SqlExpert {
    /**
     * 是否强制包裹SQL中的名称来进行关键字逃逸，默认：true
     */
    private static final boolean FORCE_WRAP_SQL_NAME = true;

    /**
     * 向指定的StringBuilder对象中添加表名或字段名字，如果是关键字则要使用包裹起来...
     *
     * @param sb   StringBuilder对象
     * @param name 表名或字段名字
     * @return 添加之后的StringBuilder对象
     */
    protected StringBuilder safeAppend(StringBuilder sb, String name) {
        if (FORCE_WRAP_SQL_NAME || SqlKeyword.isKeyword(name)) {
            final char character = wrapCharacter();
            return sb.append(character).append(name).append(character);
        } else {
            return sb.append(name);
        }
    }

    /**
     * 如果遇到关键字的情况下使用什么字符包裹
     *
     * @return 包裹关键字的字符
     */
    protected abstract char wrapCharacter();

    /**
     * 将来有其他SQL时，如果不合适，就把此层改进为通用型，具体实现放入底层重写.
     * <p>
     *
     * @param fm 属性描述对象.
     * @return 返回当前属性对应SQL中的类型.
     */
    protected String evalFieldType(FieldMapping fm) {
        switch (fm.getType()) {
            // Boolean直接写死啦，不可能为其他值的.
            case AsBoolean:
                return "BIT(1)";

            // 字符串类型的，过长需要换类型
            case AsString:
            case AsJson:
                // 如果大于65535，那就要转化为MEDIUMTEXT，大概能存~16M
                if (fm.getWidth() > DataConstant.TEXT_MAX_WIDTH) {
                    return "MEDIUMTEXT";
                }
                // 如果长度等于10K，那就转化为TEXT，大概能存~64K
                else if (fm.getWidth() >= DataConstant.VARCHAT_MAX_WIDTH) {
                    return "TEXT";
                }
                // 其他情况还是使用VarChar方式
                return "VARCHAR(" + fm.getWidth() + ")";

            // 日期类型的，三种，其他用不着就不实现啦.
            case AsInstant:
            case AsLocalDateTime:
            case AsDate:
                return "DATETIME";

            // 数字类型的就写成通用的，Mysql的由子类重写
            case AsInteger:
            case AsAtomicInteger:
            case AsLong:
            case AsLongAdder:
            case AsAtomicLong:
                // 用户自定义了宽度
                if (fm.getWidth() > 0) {
                    return "INT(" + fm.getWidth() + ")";
                }
                // 用数据库的默认宽度
                return "INT";

            case AsDouble:
            case AsFloat:
                // 用户自定义了精度
                if (fm.getWidth() > 0 && fm.getPrecision() > 0) {
                    return "NUMERIC(" + fm.getWidth() + "," + fm.getPrecision() + ")";
                }
                // 用默认精度
                if (fm.isDouble()) {
                    return "NUMERIC(15,10)";
                }
                return "FLOAT";
            case AsBlob:
                // 如果大于65535，那就要转化为MEDIUMTEXT，大概能存~16M
                if (fm.getWidth() > DataConstant.BLOB_MAX_WIDTH) {
                    return "MEDIUMBLOB";
                }
                // 小于65535，就使用Blob
                return "BLOB";
            default:
                throw new UnrealizedException("未实现的Java属性转Mysql类型：" + fm.getType());
        }
    }


    @Override
    public <T> String genInsertSql(EntityMapping<T> em) {
        // INSERT [LOW_PRIORITY | DELAYED] [IGNORE]
        // [INTO] tbl_name [(col_name,...)]
        // VALUES (expression,...),(...),...
        StringBuilder sb = new StringBuilder(128);
        sb.append("INSERT INTO ");
        this.safeAppend(sb, em.getTableName()).append(" (");

        int count = 0;
        for (FieldMapping fm : em.getFieldMapping()) {
            this.safeAppend(sb, fm.getColumnName()).append(',');
            count++;
        }
        sb.setCharAt(sb.length() - 1, ')');

        sb.append(" VALUES (");
        sb.append("?,".repeat(Math.max(0, count)));
        sb.setCharAt(sb.length() - 1, ')');
        return sb.toString();
    }

    @Override
    public <T> String genDeleteSql(EntityMapping<T> sem) {
        // delete from item where id=?
        StringBuilder sb = new StringBuilder(128);
        sb.append("DELETE FROM ");
        this.safeAppend(sb, sem.getTableName()).append(" WHERE ");
        this.safeAppend(sb, sem.getPrimaryId().getColumnName()).append("=?");
        return sb.toString();
    }

    @Override
    public <T> String genUpdateSql(EntityMapping<T> em) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("UPDATE ").append(em.getTableName()).append(" SET ");
        for (FieldMapping fm : em.getFieldMapping()) {
            if (!fm.isPrimaryId()) {
                this.safeAppend(sb, fm.getColumnName()).append("=?,");
            }
        }
        sb.setCharAt(sb.length() - 1, ' ');

        sb.append("WHERE ");
        this.safeAppend(sb, em.getPrimaryId().getColumnName()).append("=?");
        return sb.toString();
    }

    @Override
    public <T> String genSelectByPlayerId(EntityMapping<T> em) {
        // Select id from item where role_id = ?
        StringBuilder sb = new StringBuilder(128);
        sb.append("SELECT ");
        for (FieldMapping fm : em.getFieldMapping()) {
            this.safeAppend(sb, fm.getColumnName()).append(',');
        }
        sb.setCharAt(sb.length() - 1, ' ');
        sb.append("FROM ");
        this.safeAppend(sb, em.getTableName());
        if (em.getPlayerId() != null) {
            sb.append(" WHERE ");
            this.safeAppend(sb, em.getPlayerId().getColumnName()).append("=?");
        }
        return sb.toString();
    }

    @Override
    public <T> String genSelectSql(EntityMapping<T> sem) {
        // Select id from item where role_id = ?
        StringBuilder sb = new StringBuilder(128);
        sb.append("SELECT ");
        for (FieldMapping fm : sem.getFieldMapping()) {
            this.safeAppend(sb, fm.getColumnName()).append(',');
        }
        sb.setCharAt(sb.length() - 1, ' ');
        sb.append("FROM ");
        this.safeAppend(sb, sem.getTableName());
        sb.append(" WHERE ");
        this.safeAppend(sb, sem.getPrimaryId().getColumnName()).append("=?");
        return sb.toString();
    }

    @Override
    public <T> String genSelectAllSql(EntityMapping<T> sem) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("SELECT ");
        for (FieldMapping fm : sem.getFieldMapping()) {
            this.safeAppend(sb, fm.getColumnName()).append(',');
        }
        sb.setCharAt(sb.length() - 1, ' ');
        sb.append("FROM ");
        return this.safeAppend(sb, sem.getTableName()).toString();
    }

    @Override
    public <T> String genDropTableColumnSql(EntityMapping<T> em, String columnName) {
        // alter table tableName drop column xxx;
        StringBuilder sb = new StringBuilder(64);
        sb.append("ALTER TABLE ");
        this.safeAppend(sb, em.getTableName());
        sb.append(" DROP COLUMN ");
        this.safeAppend(sb, columnName);
        return sb.toString();
    }

    @Override
    public <T> String genUpdateDefaultValueSql(EntityMapping<T> em, FieldMapping fm) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("UPDATE ");
        this.safeAppend(sb, em.getTableName()).append(" SET ");
        // 使用占位符的方式更新
        this.safeAppend(sb, fm.getColumnName()).append("=?");
        return sb.toString();
    }

    // ------------------------ 带值的SQL ----------------------------

    @Override
    public <T> String genInsertSql(EntityMapping<T> em, T entity) {
        StringBuilder sb = new StringBuilder(256);
        sb.append("INSERT DELAYED INTO ");
        this.safeAppend(sb, em.getTableName()).append(" (");
        for (FieldMapping fm : em.getFieldMapping()) {
            this.safeAppend(sb, fm.getColumnName()).append(',');
        }
        sb.setCharAt(sb.length() - 1, ')');

        sb.append(" VALUES (");
        for (FieldMapping fm : em.getFieldMapping()) {
            this.handleSingleQuotationMarks(sb, em, fm, entity);
            sb.append(",");
        }
        sb.setCharAt(sb.length() - 1, ')');
        return sb.toString();
    }

    @Override
    public <T> String genUpdateSql(EntityMapping<T> em, T entity) {
        StringBuilder sb = new StringBuilder(256);
        sb.append("UPDATE ").append(em.getTableName()).append(" SET ");
        for (FieldMapping fm : em.getFieldMapping()) {
            if (!fm.isPrimaryId()) {
                this.safeAppend(sb, fm.getColumnName()).append("=");
                this.handleSingleQuotationMarks(sb, em, fm, entity);
                sb.append(",");
            }
        }
        sb.setCharAt(sb.length() - 1, ' ');

        sb.append("WHERE ");
        this.safeAppend(sb, em.getPrimaryId().getColumnName()).append("=");
        this.handleSingleQuotationMarks(sb, em, em.getPrimaryId(), entity);
        return sb.toString();
    }

    @Override
    public <T> String genDeleteSql(EntityMapping<T> em, T entity) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("DELETE FROM ");
        this.safeAppend(sb, em.getTableName()).append(" WHERE ");
        this.safeAppend(sb, em.getPrimaryId().getColumnName()).append("=");
        this.handleSingleQuotationMarks(sb, em, em.getPrimaryId(), entity);
        return sb.toString();
    }

    private <T> void handleSingleQuotationMarks(StringBuilder sb, EntityMapping<T> em, FieldMapping fm, T entity) {
        switch (fm.getType()) {
            case AsBoolean:
            case AsInteger:
            case AsAtomicInteger:
            case AsLong:
            case AsLongAdder:
            case AsAtomicLong:
            case AsFloat:
            case AsDouble:
                sb.append(em.getMethodAccess().invoke(entity, fm.getGetMethodIndex()));
                break;
            default:
                sb.append("'").append(em.getMethodAccess().invoke(entity, fm.getGetMethodIndex())).append("'");
                break;
        }
    }
}
/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.orm.accessor.sql;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Sql关键字.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 4.0
 */
public class SqlKeyword {
    private static Set<String> KEYWORD = Collections.emptySet();

    /**
     * 提取所有关键字
     *
     * @param meta 数据库元数据
     * @throws SQLException 可能会抛出SQL异常
     */
    public static void extractKeywords(DatabaseMetaData meta) throws SQLException {
        KEYWORD = new HashSet<>(Arrays.asList(meta.getSQLKeywords().toLowerCase().split(",")));
    }

    /**
     * 判定指定名称是否为关键字.
     *
     * @param name 指定名称
     * @return 如果是关键字的返回true
     */
    public static boolean isKeyword(String name) {
        return KEYWORD.contains(name);
    }
}
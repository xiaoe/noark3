/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.orm.accessor.sql.pg;

import xyz.noark.core.util.StringUtils;
import xyz.noark.orm.DataConstant;
import xyz.noark.orm.EntityMapping;
import xyz.noark.orm.accessor.sql.AbstractSqlDataAccessor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.Map;

import static xyz.noark.log.LogHelper.logger;

/**
 * postgresql数据访问类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 4.0
 */
public class PgDataAccessor extends AbstractSqlDataAccessor {
    /**
     * PG字段数据过长异常类名称
     */
    private static final String PG_ERROR_VALUE_TOO_LONG_PREFIX = "ERROR: value too long";

    public PgDataAccessor(DataSource dataSource) {
        super(new PgSqlExpert(), dataSource);
    }

    @Override
    protected boolean tryFixDataSaveException(boolean flag, EntityMapping<?> em, Map<String, Integer> columnMaxLenMap, Exception e) {
        // 1.尝试修复数据库字段过长的问题，自动扩容
        // Caused by: org.postgresql.util.PSQLException: ERROR: value too long for type character varying(5)
        if (flag && autoAlterColumnLength && e.getMessage().startsWith(PG_ERROR_VALUE_TOO_LONG_PREFIX)) {
            synchronized (this) {
                this.handleDataTooLongException(em, columnMaxLenMap);
            }
            return true;
        }

        return false;
    }



}
/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.orm.accessor.sql.mysql;

import xyz.noark.orm.EntityMapping;
import xyz.noark.orm.accessor.sql.AbstractSqlDataAccessor;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Map;

/**
 * MySQL数据访问类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class MysqlDataAccessor extends AbstractSqlDataAccessor {
    /**
     * MYSQL字段数据过长异常类名称
     */
    private static final String MYSQL_DATA_TRUNCATION_CLASS_SIMPLE_NAME = "MysqlDataTruncation";
    /**
     * MYSQL存档时字符串值不正确，基本认定为Emoji表情搞得鬼
     */
    private static final String MYSQL_DATA_INCORRECT_PREFIX = "Incorrect string value:";

    public MysqlDataAccessor(DataSource dataSource) {
        super(new MysqlSqlExpert(), dataSource);
    }

    @Override
    protected boolean tryFixDataSaveException(boolean flag, EntityMapping<?> em, Map<String, Integer> columnMaxLenMap, Exception e) {
        // 1.尝试修复数据库字段过长的问题，自动扩容
        // Caused by: com.mysql.jdbc.MysqlDataTruncation: Data truncation: Data too long for column 'json' at row 1
        // Caused by: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column 'name' at row 1
        // 不同版本的驱动，截断异常的名称是一样的，但他的包名不一样，所以判定时需要注意
        if (flag && autoAlterColumnLength && MYSQL_DATA_TRUNCATION_CLASS_SIMPLE_NAME.equals(e.getClass().getSimpleName())) {
            synchronized (this) {
                this.handleDataTooLongException(em, columnMaxLenMap);
            }
            return true;
        }

        // 2.尝试修复Emoji表情存档失败的问题
        // java.sql.SQLException: Incorrect string value: '\xF0\x9F\x98\xA218' for column 'content' at row 1
        if (flag && autoAlterEmojiColumn && e instanceof SQLException && e.getMessage().startsWith(MYSQL_DATA_INCORRECT_PREFIX)) {
            synchronized (this) {
                em.getFieldMapping().forEach(v -> v.setEmoji(true));
            }
            return true;
        }

        return false;
    }
}
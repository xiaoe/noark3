/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.orm.accessor.sql.mysql;

import xyz.noark.core.util.StringUtils;
import xyz.noark.orm.DataConstant;
import xyz.noark.orm.EntityMapping;
import xyz.noark.orm.FieldMapping;
import xyz.noark.orm.accessor.FieldType;
import xyz.noark.orm.accessor.sql.AbstractSqlExpert;

/**
 * Mysql
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class MysqlSqlExpert extends AbstractSqlExpert {

    @Override
    public <T> String genCreateTableSql(EntityMapping<T> em) {
        StringBuilder sb = new StringBuilder(512);
        sb.append("CREATE TABLE ");
        this.safeAppend(sb, em.getTableName()).append(" (");

        // 创建字段
        for (FieldMapping fm : em.getFieldMapping()) {
            sb.append('\n');
            this.safeAppend(sb, fm.getColumnName()).append(' ').append(evalFieldType(fm));
            // 如果指定了排序规则，那也要加进去.
            this.buildCollate(sb, fm);

            // 主键的 @Id，应该加入唯一性约束
            if (fm.isPrimaryId()) {
                sb.append(" UNIQUE NOT NULL");
                // 自增主键
                if (fm.hasGeneratedValue()) {
                    sb.append(" AUTO_INCREMENT");
                }
            }
            // 普通字段
            else {
                // 下面的关于Timestamp处理，是因为MySql中第一出现Timestamp的话，如果没有设定default，数据库默认会设置为CURRENT_TIMESTAMP
                if (fm.isNotNull()) {
                    sb.append(" NOT NULL");
                } else if (fm.getType() == FieldType.AsDate) {
                    sb.append(" NULL");
                }

                if (fm.hasDefaultValue()) {
                    switch (fm.getType()) {
                        case AsBoolean:
                        case AsInteger:
                        case AsAtomicInteger:
                        case AsLong:
                        case AsLongAdder:
                        case AsAtomicLong:
                        case AsFloat:
                        case AsDouble:
                            sb.append(" DEFAULT ").append(fm.getDefaultValue());
                            break;
                        // Blob是不可以有默认值的
                        case AsBlob:
                            break;
                        default:
                            // 超过这个值当Text啦，Text是不可以有默认值的.
                            if (fm.getWidth() < DataConstant.VARCHAT_MAX_WIDTH) {
                                sb.append(" DEFAULT '").append(fm.getDefaultValue()).append("'");
                            }
                            break;
                    }
                }
            }

            if (fm.hasColumnComment()) {
                sb.append(" COMMENT '").append(fm.getColumnComment()).append("'");
            }

            sb.append(',');
        }
        // 创建主键
        FieldMapping pk = em.getPrimaryId();
        if (pk != null) {
            sb.append('\n');
            sb.append("PRIMARY KEY (");
            this.safeAppend(sb, pk.getColumnName()).append(')');
        }
        // 玩家ID的索引
        if (em.getPlayerId() != null && pk != null && !em.getPlayerId().getField().equals(pk.getField())) {
            sb.append(',').append('\n');
            sb.append("INDEX INDEX_UD (");
            this.safeAppend(sb, em.getPlayerId().getColumnName()).append(')');
        }
        // 结束表字段设置并设置特殊引擎
        sb.append("\n)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4");
        // 表名注释
        if (!StringUtils.isEmpty(em.getTableComment())) {
            sb.append(" COMMENT='").append(em.getTableComment()).append("'");
        }
        return sb.append(";").toString();
    }

    /**
     * 构建排序规则信息.
     *
     * @param sb SQL拼接字符串
     * @param fm 当前属性
     */
    private void buildCollate(StringBuilder sb, FieldMapping fm) {
        if (fm.hasCollate()) {
            sb.append(" COLLATE ").append(fm.getCollateValue());
        }
    }

    @Override
    protected char wrapCharacter() {
        return '`';
    }

    @Override
    protected String evalFieldType(FieldMapping fm) {
        return switch (fm.getType()) {
            // 游戏嘛，数字就是int(11)不要想多啦，简单直接明了
            case AsInteger, AsAtomicInteger -> "INT(11)";

            // 20就20吧~~~
            case AsLong, AsLongAdder, AsAtomicLong -> "BIGINT(20)";

            // 有小数的就直接写上他写的参数
            case AsDouble -> "DOUBLE(" + fm.getPrecision() + "," + fm.getScale() + ")";
            case AsFloat -> "FLOAT(" + fm.getPrecision() + "," + fm.getScale() + ")";

            // 其它的参照默认字段规则 ...
            default -> super.evalFieldType(fm);
        };
    }

    @Override
    public <T> String genAddTableColumnSql(EntityMapping<T> em, FieldMapping fm) {
        return genAddOrUpdateTableColumnSql(em, fm, false);
    }

    @Override
    public <T> String genUpdateTableColumnSql(EntityMapping<T> em, FieldMapping fm) {
        return genAddOrUpdateTableColumnSql(em, fm, true);
    }

    private <T> String genAddOrUpdateTableColumnSql(EntityMapping<T> em, FieldMapping fm, boolean update) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("ALTER TABLE ");
        this.safeAppend(sb, em.getTableName());
        sb.append(update ? " MODIFY" : " ADD").append(" COLUMN ");
        this.safeAppend(sb, fm.getColumnName());
        sb.append(' ').append(evalFieldType(fm));

        // 如果指定了排序规则，那也要加进去.
        this.buildCollate(sb, fm);

        if (fm.isNotNull()) {
            sb.append(" NOT NULL");
        } else if (fm.getType() == FieldType.AsDate) {
            sb.append(" NULL");
        }

        if (fm.hasDefaultValue()) {
            switch (fm.getType()) {
                case AsBoolean:
                case AsInteger:
                case AsAtomicInteger:
                case AsLong:
                case AsLongAdder:
                case AsAtomicLong:
                case AsFloat:
                case AsDouble:
                    sb.append(" DEFAULT ").append(fm.getDefaultValue());
                    break;
                //Blob是不会有默认值的
                case AsBlob:
                    break;
                default:
                    // 超过这个值当Text啦，Text是不可以有默认值的.
                    if (fm.getWidth() < DataConstant.VARCHAT_MAX_WIDTH) {
                        sb.append(" DEFAULT '").append(fm.getDefaultValue()).append("'");
                    }
                    break;
            }
        }

        // 自增主键
        if (fm.hasGeneratedValue()) {
            sb.append(" AUTO_INCREMENT");
        }

        // 字段描述
        if (fm.hasColumnComment()) {
            sb.append(" COMMENT '").append(fm.getColumnComment()).append("'");
        }
        return sb.toString();
    }
}
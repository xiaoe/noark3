package com.company.game.domain;

import xyz.noark.core.annotation.PlayerId;
import xyz.noark.core.annotation.orm.*;

import java.util.Date;

@Entity
@Table(name = "player_item_10", comment = "道具表")
public class PlayerItem {

    @Id
    @Column(name = "id", comment = "道具ID")
    private Long id;

    @PlayerId
    @Column(name = "player_id", nullable = false, comment = "玩家ID")
    private Long playerId;

    @Column(name = "template_id", length = 5, nullable = false, defaultValue = "0", comment = "道具ID")
    private String templateId;

    @Column(name = "level", nullable = false, defaultValue = "1", comment = "等级")
    private int level;

    @Column(name = "select", nullable = false, defaultValue = "true", comment = "测试关键字")
    private boolean select;

    @CreatedDate
    @Column(name = "create_time", nullable = false, comment = "创建时间", defaultValue = "2018-07-06 05:04:03")
    private Date createTime;
    @LastModifiedDate
    @Column(name = "modified_time", nullable = false, comment = "最后修改时间", defaultValue = "2018-07-06 05:04:03")
    private Date modifiedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }



    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}

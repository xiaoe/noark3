package com.company.game.domain;

import xyz.noark.core.annotation.Repository;
import xyz.noark.core.util.RandomUtils;
import xyz.noark.orm.repository.MultiCacheRepository;

import javax.annotation.PostConstruct;
import java.util.Date;

@Repository
public class PlayerItemRepository extends MultiCacheRepository<PlayerItem, Long> {
    @PostConstruct
    public void test() {
        long id = RandomUtils.nextLong(10000000);
        PlayerItem entity = this.load(id);


        if (entity == null) {
            entity = new PlayerItem();
            entity.setId(id);
            entity.setPlayerId(2L);
            entity.setTemplateId("\uD83D\uDE00");
            entity.setCreateTime(new Date());
            entity.setModifiedTime(entity.getCreateTime());
            this.insert(entity);
        }


        entity.setTemplateId("xxxxx");
        entity.setModifiedTime(new Date());
        this.update(entity);


        this.delete(entity);
    }
}

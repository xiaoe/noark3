/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.game.config;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 基于账号密码方案
 *
 * @author 小流氓[176543888@qq.com]
 * @since 4.0
 */
public class NacosServerByPassword extends NacosServerInfo {
    private final String username;
    private final String password;

    public NacosServerByPassword(List<String> serverAddrList, String username, String password, String tenant) {
        super(serverAddrList, tenant);
        this.username = username;
        this.password = password;
    }

    @Override
    protected Map<String, String> doAuthExtra(StringBuilder sb) {
        // 有配置账号密码
        sb.append("&username=").append(username);
        sb.append("&password=").append(password);

        // 账号密码是通用拼接URL实现的，就不需要返回了...
        return Collections.emptyMap();
    }
}

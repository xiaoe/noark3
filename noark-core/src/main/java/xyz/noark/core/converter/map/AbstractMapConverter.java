/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.core.converter.map;

import xyz.noark.core.converter.AbstractArrayConverter;
import xyz.noark.core.converter.AbstractConverter;
import xyz.noark.core.converter.ConvertManager;
import xyz.noark.core.converter.Converter;
import xyz.noark.core.exception.UnrealizedException;
import xyz.noark.core.util.FieldUtils;
import xyz.noark.core.util.MapUtils;
import xyz.noark.core.util.StringUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.Collections;
import java.util.Map;

/**
 * 抽象的Map转化器
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.4.8
 */
public abstract class AbstractMapConverter extends AbstractArrayConverter {
    protected String buildErrorMsg() {
        return "Map结构. 例=key1:value1,key2:value2,key3:value3";
    }

    protected Map<Object, Object> doConvert(String value, AnnotatedElement element, int depth) throws Exception {
        // 没值那就给个空的Map
        if (StringUtils.isBlank(value)) {
            return Collections.emptyMap();
        }

        AbstractConverter<?> keyGenericConverter = (AbstractConverter<?>) this.getMapKeyGenericConverter(element);
        AbstractConverter<?> valueGenericConverter = (AbstractConverter<?>) this.getMapValueGenericConverter(element);

        // 第一层，使用,分隔
        String[] array = this.splitValueByDepth(value, element, depth, StringUtils.COMMA);

        Map<Object, Object> result = this.createMap(array.length);
        for (String s : array) {
            // 如果多了一个分隔符则忽略那些空的情况
            if (StringUtils.isBlank(s)) {
                continue;
            }

            // 第2层，使用:分隔
            String[] split = this.splitValueByDepth(s, element, depth + 1, StringUtils.COLON);

            // 对象算是第3层
            Object keyObject = keyGenericConverter.doConvert(split[0], element, depth + 2);
            Object valueObject = valueGenericConverter.doConvert(split[1], element, depth + 2);
            result.put(keyObject, valueObject);
        }
        return result;
    }

    private Converter<?> getMapValueGenericConverter(AnnotatedElement element) {
        // 属性
        if (element instanceof Field) {
            return this.getMapValueGenericConverter((Field) element);
        }
        // 参数
        else if (element instanceof Parameter) {
            return this.getMapValueGenericConverter((Parameter) element);
        }
        // 非法类型
        throw new UnrealizedException("未知类型的转换器. element=(" + element.getClass().getName());
    }

    private Converter<?> getMapKeyGenericConverter(AnnotatedElement element) {
        // 属性
        if (element instanceof Field) {
            return this.getMapKeyGenericConverter((Field) element);
        }
        // 参数
        else if (element instanceof Parameter) {
            return this.getMapKeyGenericConverter((Parameter) element);
        }
        // 非法类型
        throw new UnrealizedException("未知类型的转换器. element=(" + element.getClass().getName());
    }

    public Map<Object, Object> doConvert(Map<String, String> dataMap, Field field, int depth) throws Exception {
        if (MapUtils.isEmpty(dataMap)) {
            return Collections.emptyMap();
        }

        AbstractConverter<?> keyGenericConverter = (AbstractConverter<?>) this.getMapKeyGenericConverter(field);
        AbstractConverter<?> valueGenericConverter = (AbstractConverter<?>) this.getMapValueGenericConverter(field);

        Map<Object, Object> result = this.createMap(32);
        for (String value : dataMap.values()) {
            // 第1层，使用,分隔
            String[] array = this.splitValueByDepth(value, field, depth, StringUtils.COMMA);
            for (String s : array) {
                // 第2层，使用:分隔
                String[] split = this.splitValueByDepth(s, field, depth + 1, StringUtils.COLON);
                // 对象算作第3层
                Object keyObject = keyGenericConverter.doConvert(split[0], field, depth + 2);
                Object valueObject = valueGenericConverter.doConvert(split[1], field, depth + 2);
                result.put(keyObject, valueObject);
            }
        }
        return result;
    }

    private Converter<?> getMapValueGenericConverter(Parameter parameter) {
        Class<?> genericClass = FieldUtils.getGenericClass(parameter.getParameterizedType(), 1);
        Converter<?> converter = ConvertManager.getInstance().getConverter(genericClass);
        if (converter == null) {
            throw new UnrealizedException("Map转换器Value未实现的类型. field=(" + parameter.getType().getName() + ")" + parameter.getName());
        }
        return converter;
    }

    private Converter<?> getMapKeyGenericConverter(Parameter parameter) {
        Class<?> genericClass = FieldUtils.getGenericClass(parameter.getParameterizedType(), 0);
        Converter<?> converter = ConvertManager.getInstance().getConverter(genericClass);
        if (converter == null) {
            throw new UnrealizedException("Map转换器Key未实现的类型. field=(" + parameter.getType().getName() + ")" + parameter.getName());
        }
        return converter;
    }

    private Converter<?> getMapValueGenericConverter(Field field) {
        Class<?> keyGenericClass = FieldUtils.getMapValueGenericClass(field);
        Converter<?> converter = ConvertManager.getInstance().getConverter(keyGenericClass);
        if (converter == null) {
            throw new UnrealizedException("Map转换器Value未实现的类型. field=(" + field.getType().getName() + ")" + field.getName());
        }
        return converter;
    }

    private Converter<?> getMapKeyGenericConverter(Field field) {
        Class<?> keyGenericClass = FieldUtils.getMapKeyGenericClass(field);
        Converter<?> converter = ConvertManager.getInstance().getConverter(keyGenericClass);
        if (converter == null) {
            throw new UnrealizedException("Map转换器Key未实现的类型. field=(" + field.getType().getName() + ")" + field.getName());
        }
        return converter;
    }

    protected abstract Map<Object, Object> createMap(int length);
}

/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.core.converter;

import xyz.noark.core.annotation.tpl.TplAttrDelimiter;
import xyz.noark.core.util.ArrayUtils;
import xyz.noark.core.util.StringUtils;

import java.lang.reflect.AnnotatedElement;

/**
 * 抽象的数组转化器，主要用来切割分组
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.4.8
 */
public abstract class AbstractArrayConverter {

    /**
     * 根据目标注解配置深度对应的分隔符来切割字符串，从而转化为对象
     *
     * @param value                 要切割的字符串
     * @param element               注解配置的元素（可能会为null）
     * @param depth                 对应的深度
     * @param defaultDelimiterChars 如果没有配置注解则使用此默认的分隔符
     * @return 返回切割后的字符串数组
     */
    protected String[] splitValueByDepth(String value, AnnotatedElement element, int depth, String defaultDelimiterChars) {
        // 分隔符
        String delimiterChars = defaultDelimiterChars;

        // 查找指定深度的分隔符
        if (element != null) {
            TplAttrDelimiter[] delimiters = element.getAnnotationsByType(TplAttrDelimiter.class);
            if (ArrayUtils.isNotEmpty(delimiters)) {
                for (TplAttrDelimiter delimiter : delimiters) {
                    if (delimiter.depth() == depth) {
                        delimiterChars = delimiter.value();
                        break;
                    }
                }
            }
        }

        // 有则使用配置分隔符，没有则使用默认分隔符
        return StringUtils.split(value, delimiterChars);
    }
}

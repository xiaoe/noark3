/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.core.util;

import xyz.noark.core.exception.ServerBootstrapException;

import java.net.*;
import java.util.Enumeration;

/**
 * IP相关操作工具类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.2
 */
public class IpUtils {

    private static final int IP_LOOP_NUM = 3;
    private static final String LOCAL_IP = "127.0.0.1";
    /**
     * 私有IP：
     * <p>
     * A类 10.0.0.0-10.255.255.255 <br>
     * B类 172.16.0.0-172.31.255.255 <br>
     * C类 192.168.0.0-192.168.255.255<br>
     **/
    private static final long A_BEGIN = ipToLong("10.0.0.0");
    private static final long A_END = ipToLong("10.255.255.255");
    private static final long B_BEGIN = ipToLong("172.16.0.0");
    private static final long B_END = ipToLong("172.31.255.255");
    private static final long C_BEGIN = ipToLong("192.168.0.0");
    private static final long C_END = ipToLong("192.168.255.255");

    /**
     * IP转化为Long类型的数字.
     *
     * @param ipAddress IP地址
     * @return Long类型的数字
     */
    public static long ipToLong(String ipAddress) {
        final String[] array = StringUtils.split(ipAddress, ".");
        long result = 0;
        for (int i = IP_LOOP_NUM; i >= 0; i--) {
            result |= Long.parseLong(array[3 - i]) << (i * 8);
        }
        return result;
    }

    /**
     * IP转化为Long类型的数字.
     *
     * @param address IP地址
     * @return Long类型的数字
     */
    public static long ipToLong(byte[] address) {
        long result = 0;
        for (int i = IP_LOOP_NUM; i >= 0; i--) {
            result |= (long) (address[3 - i] & 0xFF) << (i * 8);
        }
        return result;
    }

    /**
     * 判定一个IP是否为内网IP.
     * <p>
     * 除了上面的IP还包含了127.0.0.1
     *
     * @param ipAddress IP地址
     * @return 如果是则返回true, 否则返回false
     */
    public static boolean isInnerIp(String ipAddress) {
        final long ipNum = ipToLong(ipAddress);
        return isInner(ipNum, A_BEGIN, A_END) || isInner(ipNum, B_BEGIN, B_END) || isInner(ipNum, C_BEGIN, C_END) || LOCAL_IP.equals(ipAddress);
    }

    /**
     * 判定一个IP地址在否在指定区间内.
     *
     * @param ipNum IP地址
     * @param begin 指定区间开始值
     * @param end   指定区间结束值
     * @return 如果在区间内则返回true, 否则返回false
     */
    private static boolean isInner(long ipNum, long begin, long end) {
        return (ipNum >= begin) && (ipNum <= end);
    }

    /**
     * 获取套接字地址的IP
     *
     * @param address 套接字地址
     * @return 返回目标IP
     */
    public static String getIp(SocketAddress address) {
        return ((InetSocketAddress) address).getAddress().getHostAddress();
    }

    /**
     * 获取本地内网IP地址并以文本形式返回IP字符串。
     *
     * @return 本地内网地址IP字符串
     * @throws SocketException 如果发生I/O错误时会抛出此异常
     */
    public static String getLocalHostAddress() throws SocketException {
        return getLocalInetAddress().getHostAddress();
    }

    /**
     * 获取本地内网地址的原始IP地址。
     * <p>
     * 结果按网络字节顺序排列：地址的最高顺序字节在getLocalAddress()[0]中
     * </p>
     *
     * @return 本地内网地址
     * @throws SocketException 如果发生I/O错误时会抛出此异常
     */
    public static byte[] getLocalAddress() throws SocketException {
        return getLocalInetAddress().getAddress();
    }

    private static InetAddress getLocalInetAddress() throws SocketException {
        // 获取计算机上的所有IP地址
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface ni = interfaces.nextElement();
            // 环回地址，忽略
            if (ni.isLoopback()) {
                continue;
            }
            // 虚拟地址，忽略
            // 通常，接口的名称将是父接口的名称，后跟冒号(:)和标识子接口的数字，因为单个物理接口可以连接多个虚拟接口
            if (ni.isVirtual()) {
                continue;
            }
            // 非(已启动并正在运行)的，也要忽略
            if (!ni.isUp()) {
                continue;
            }

            // 满足条件中选择第一个Ipv4地址
            Enumeration<InetAddress> addresses = ni.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress address = addresses.nextElement();
                if (address instanceof Inet4Address) {
                    return address;
                }
            }
        }

        // 有些机器情况特殊啊，抛个启动异常，待研究新环境
        throw new ServerBootstrapException("无法获取内网IP地址");
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getLocalHostAddress());
    }
}
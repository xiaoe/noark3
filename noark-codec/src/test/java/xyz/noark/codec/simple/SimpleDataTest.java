/*
 * Copyright © 2018 huiyunetwork.com All Rights Reserved.
 *
 * 感谢您加入辉娱网络，不用多久，您就会升职加薪、当上总经理、出任CEO、迎娶白富美、从此走上人生巅峰
 * 除非符合本公司的商业许可协议，否则不得使用或传播此源码，您可以下载许可协议文件：
 *
 * 		http://www.huiyunetwork.com/LICENSE
 *
 * 1、未经许可，任何公司及个人不得以任何方式或理由来修改、使用或传播此源码;
 * 2、禁止在本源码或其他相关源码的基础上发展任何派生版本、修改版本或第三方版本;
 * 3、无论你对源代码做出任何修改和优化，版权都归辉娱网络所有，我们将保留所有权利;
 * 4、凡侵犯辉娱网络相关版权或著作权等知识产权者，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.simple;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONB;
import com.alibaba.fastjson2.util.JDKUtils;
import org.apache.fury.Fury;
import org.apache.fury.config.Language;
import org.apache.fury.logging.LoggerFactory;
import org.junit.Test;
import xyz.noark.codec.encoder.DefaultBufferedEncoder;
import xyz.noark.codec.encoder.ObjectEncoder;
import xyz.noark.codec.simple.data.SimpleData;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @author 小流氓[176543888@qq.com]
 */
public class SimpleDataTest {
    public static SimpleData data;
    static ObjectEncoder<SimpleData> encoder = new ObjectEncoderImplSimpleDataGen();

    static {
        data = new SimpleData();
        data.setId(1);
        data.setTemplateId(2);
        data.setName("");
        data.setFlag(false);
    }


    @Test
    public void testEncode() throws IOException {
        LoggerFactory.disableLogging();
        byte[] array = encode(data);
        System.out.println(Arrays.toString(array));
        System.out.println(array.length + "\t" + new String(array));

        byte[] jsonb = JSONB.toBytes(data);
        System.out.println(jsonb.length + "\t" + new String(jsonb));


        {
            Fury fury = Fury.builder().withLanguage(Language.JAVA)
                    .withRefTracking(true).requireClassRegistration(false).build();
            byte[] result = fury.serialize(data);
            SimpleData simpleData = (SimpleData) fury.deserialize(result);


            System.out.println(result.length + "\t" + new String(result));

            System.out.println(JSON.toJSONString(simpleData));
        }

        String str = "abc";
        System.out.println(Arrays.toString(str.getBytes(StandardCharsets.UTF_8)));
    }


    static byte[] buffer = new byte[10240];

    public static byte[] encode(SimpleData data) throws IOException {
        try (var os = new DefaultBufferedEncoder(buffer)) {
            encoder.encode(os, data);
            //context.writeName(os);
            return os.toByteArray();
        }
    }


    @Test
    public void testX() {
        String value = "123";
        System.out.println("Lambda:" + Arrays.toString(JDKUtils.STRING_VALUE.apply(value)));
        System.out.println("getBytes:" + Arrays.toString(value.getBytes()));
        System.out.println("getBytes8:" + Arrays.toString(value.getBytes(StandardCharsets.UTF_8)));
        System.out.println("getBytes16:" + Arrays.toString(value.getBytes(StandardCharsets.UTF_16)));
    }
}
/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.bp.proto.req;

import com.zfoo.protocol.anno.Protocol;

/**
 * 参战玩家信息.
 *
 * @author 小流氓[176543888@qq.com]
 */
@Protocol(id = 15)
public class FightPlayerDTO {
    /**
     * 玩家ID
     */
    private Long id;
    /**
     * 玩家名称
     */
    private String name;
    /**
     * 玩家头像+边框
     */
    private ShowData show;
    /**
     * 科技加成
     */
    private TechInfo techInfo;
    /**
     * 玩家形象
     */
    private int vividId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShowData getShow() {
        return show;
    }

    public void setShow(ShowData show) {
        this.show = show;
    }

    public TechInfo getTechInfo() {
        return techInfo;
    }

    public void setTechInfo(TechInfo techInfo) {
        this.techInfo = techInfo;
    }

    public int getVividId() {
        return vividId;
    }

    public void setVividId(int vividId) {
        this.vividId = vividId;
    }
}

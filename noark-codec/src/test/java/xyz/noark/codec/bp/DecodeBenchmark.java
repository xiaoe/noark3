/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.bp;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONB;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zfoo.protocol.ProtocolManager;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.buffer.UnpooledHeapByteBuf;
import org.apache.fury.Fury;
import org.apache.fury.config.Language;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import xyz.noark.codec.bp.proto.FightReq;
import xyz.noark.codec.bp.proto.req.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 反序列化性能大比拼。
 * <p>
 * JMH Visualizer: <a href="https://jmh.morethan.io/">JMH Visualizer</a>
 *
 * @author 小流氓[176543888@qq.com]
 */
@Fork(1)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 10, time = 1)
@OutputTimeUnit(TimeUnit.SECONDS)
@BenchmarkMode({Mode.Throughput})
@State(Scope.Benchmark)
public class DecodeBenchmark {
    private FightReq req;
    private ObjectMapper mapper;
    private Fury fury;
    private ByteBuf buffer;


    private byte[] fastjson2Data;
    private byte[] furyData;
    private byte[] zfooData;

    @Setup(Level.Trial)
    public void init() throws IOException {
        ClassLoader classLoader = EncodeBenchmark.class.getClassLoader();
        try (InputStream is = classLoader.getResourceAsStream("proto/fight.req.json")) {
            req = JSON.parseObject(is, FightReq.class);
        }

        Set<Class<?>> classes = Set.of(FightReq.class, FightArmsDTO.class, FightHeroDTO.class,
                FightSkillExtendDTO.class, FightLocationDTO.class, FightNpcDTO.class, FightNpcHeroDTO.class,
                FightNpcTroopsDTO.class, FightPlayDTO.class, FightPlayerDTO.class, FightPlayerTroopsDTO.class,
                FightSkillDTO.class, FightTroopsBuffDTO.class,
                FightTroopsDTO.class, ShowData.class, TechCondition.class, TechInfo.class, TechEffect.class
        );

        {// fastjson2
            this.fastjson2Data = JSONB.toBytes(req);
            System.out.println();
            System.out.println("fastjson2 len=" + fastjson2Data.length);
            System.out.println();
            FightReq fightReq = JSONB.parseObject(fastjson2Data, FightReq.class);
            //System.out.println(JSON.toJSONString(fightReq));
        }

        { // fury
            this.fury = Fury.builder().withLanguage(Language.JAVA)
                    .withRefTracking(false)
                    .requireClassRegistration(false)
                    .withNumberCompressed(true)
                    .build();

            for (Class<?> aClass : classes) {
                fury.register(aClass);
            }

            this.furyData = fury.serializeJavaObject(req);
            System.out.println();
            System.out.println("fury len=" + furyData.length);
            System.out.println();
            FightReq fightReq = fury.deserializeJavaObject(furyData, FightReq.class);
            //System.out.println(JSON.toJSONString(fightReq));
        }

        {// zfoo
            // netty的ByteBuf做了更多的安全检测，java自带的ByteBuffer并没有做安全检测，为了公平，把不需要的检测去掉
            // java通过ByteBuffer.allocate(1024 * 8)构造出来的是使用了unsafe的HeapByteBuffer，为了公平，使用netty中带有unsafe操作的UnpooledUnsafeHeapByteBuf
            System.setProperty("io.netty.buffer.checkAccessible", "true");
            System.setProperty("io.netty.buffer.checkBounds", "true");

            this.buffer = new UnpooledHeapByteBuf(ByteBufAllocator.DEFAULT, 100, 1_0000);

            // zfoo协议注册，只能初始化一次
            ProtocolManager.initProtocol(classes);

            buffer.clear();
            ProtocolManager.write(buffer, req);
            this.zfooData = buffer.array();
            System.out.println();
            System.out.println("zfoo len=" + zfooData.length);
            System.out.println();

            ByteBuf byteBuf = Unpooled.wrappedBuffer(zfooData);
            Object read = ProtocolManager.read(byteBuf);
            //System.out.println(JSON.toJSONString(read));
        }
    }


    @Benchmark
    public void fastJson2(Blackhole bh) {
        bh.consume(JSONB.parseObject(fastjson2Data, FightReq.class));
    }

    @Benchmark
    public void fury(Blackhole bh) {
        bh.consume(fury.deserializeJavaObject(furyData, FightReq.class));
    }

    @Benchmark
    public void zfoo(Blackhole bh) {
        ByteBuf byteBuf = Unpooled.wrappedBuffer(zfooData);
        bh.consume(ProtocolManager.read(byteBuf));
    }

    public static void main(String[] args) throws Exception {
        Options options = new OptionsBuilder()
                .include(DecodeBenchmark.class.getName())
                .resultFormat(ResultFormatType.JSON).result("result.json")
                .build();
        new Runner(options).run();
    }
}
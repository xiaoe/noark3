/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.bp.proto.req;

import com.zfoo.protocol.anno.Protocol;

import java.util.List;
import java.util.Map;

/**
 * 参战军团信息
 *
 * @author 小流氓[176543888@qq.com]
 */
@Protocol(id = 4)
public class FightTroopsDTO {
    /**
     * 军团对应的玩家，可能是NPC，没值
     */
    private FightPlayerDTO player;
    /**
     * 军团对应的NPC，可能是玩家没值
     */
    private FightNpcDTO npc;

    /**
     * 本场战斗时士气值
     */
    private int morale;

    /**
     * 军团相关数据(与Npc互斥)
     */
    private FightPlayerTroopsDTO playerTroops;
    /**
     * Npc相关数据(与军团互斥)
     */
    private FightNpcTroopsDTO npcTroops;

    // ------------------------------------------
    /**
     * 部队阵形Buff信息
     */
    private FightTroopsBuffDTO fightTroopsBuffDTO;
    /**
     * 部队私有全局效果
     */
    private TechInfo techInfo;
    /**
     * 全局加成修正属性
     */
    private Map<String, Long> replaceTech;
    /**
     * 扩展技能，每个英雄都有
     */
    private List<FightSkillExtendDTO> extendSkillList;
    /**
     * 部队技能，首个英雄有
     */
    private List<FightSkillExtendDTO> troopSkillList;

    public FightPlayerDTO getPlayer() {
        return player;
    }

    public void setPlayer(FightPlayerDTO player) {
        this.player = player;
    }

    public FightNpcDTO getNpc() {
        return npc;
    }

    public void setNpc(FightNpcDTO npc) {
        this.npc = npc;
    }

    public int getMorale() {
        return morale;
    }

    public void setMorale(int morale) {
        this.morale = morale;
    }

    public FightPlayerTroopsDTO getPlayerTroops() {
        return playerTroops;
    }

    public void setPlayerTroops(FightPlayerTroopsDTO playerTroops) {
        this.playerTroops = playerTroops;
    }

    public FightNpcTroopsDTO getNpcTroops() {
        return npcTroops;
    }

    public void setNpcTroops(FightNpcTroopsDTO npcTroops) {
        this.npcTroops = npcTroops;
    }

    public Map<String, Long> getReplaceTech() {
        return replaceTech;
    }

    public void setReplaceTech(Map<String, Long> replaceTech) {
        this.replaceTech = replaceTech;
    }

    public List<FightSkillExtendDTO> getExtendSkillList() {
        return extendSkillList;
    }

    public void setExtendSkillList(List<FightSkillExtendDTO> extendSkillList) {
        this.extendSkillList = extendSkillList;
    }

    public TechInfo getTechInfo() {
        return techInfo;
    }

    public void setTechInfo(TechInfo techInfo) {
        this.techInfo = techInfo;
    }

    public FightTroopsBuffDTO getFightTroopsBuffDTO() {
        return fightTroopsBuffDTO;
    }

    public void setFightTroopsBuffDTO(FightTroopsBuffDTO fightTroopsBuffDTO) {
        this.fightTroopsBuffDTO = fightTroopsBuffDTO;
    }

    public List<FightSkillExtendDTO> getTroopSkillList() {
        return troopSkillList;
    }

    public void setTroopSkillList(List<FightSkillExtendDTO> troopSkillList) {
        this.troopSkillList = troopSkillList;
    }
}

/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.bp.proto.req;

import com.zfoo.protocol.anno.Protocol;

import java.util.List;
import java.util.Map;

/**
 * 战斗请求中的英雄.
 *
 * @author 小流氓[176543888@qq.com]
 */
@Protocol(id = 7)
public class FightHeroDTO {
    // 武将位置
    private int index;
    // 武将编号
    private int templateId;
    // 武将等级
    private int level;
    // 武将星级
    private int star;
    // 战力
    private long power;
    // 出战生物
    private FightArmsDTO arms;
    // 当前数量
    private int curNum;
    // 最大数量
    private int maxNum;
    // 武将属性
    private Map<String, Long> attrMap;
    // 战斗技能
    private List<FightSkillDTO> skillList;
    // 英雄私有全局效果
    private TechInfo techInfo;
    // 缘分技能
    private List<FightSkillExtendDTO> fateSkillList;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public long getPower() {
        return power;
    }

    public void setPower(long power) {
        this.power = power;
    }

    public FightArmsDTO getArms() {
        return arms;
    }

    public void setArms(FightArmsDTO arms) {
        this.arms = arms;
    }

    public int getCurNum() {
        return curNum;
    }

    public void setCurNum(int curNum) {
        this.curNum = curNum;
    }

    public int getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(int maxNum) {
        this.maxNum = maxNum;
    }

    public Map<String, Long> getAttrMap() {
        return attrMap;
    }

    public void setAttrMap(Map<String, Long> attrMap) {
        this.attrMap = attrMap;
    }

    public List<FightSkillDTO> getSkillList() {
        return skillList;
    }

    public void setSkillList(List<FightSkillDTO> skillList) {
        this.skillList = skillList;
    }

    public TechInfo getTechInfo() {
        return techInfo;
    }

    public void setTechInfo(TechInfo techInfo) {
        this.techInfo = techInfo;
    }

    public List<FightSkillExtendDTO> getFateSkillList() {
        return fateSkillList;
    }

    public void setFateSkillList(List<FightSkillExtendDTO> fateSkillList) {
        this.fateSkillList = fateSkillList;
    }
}

package xyz.noark.codec.along;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONB;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.github.houbb.data.factory.core.util.DataUtil;
import xyz.noark.codec.along.data.SkillFire_S2C_Msg;
import xyz.noark.codec.along.data.SkillFire_S2C_Msg2;

import java.io.IOException;
import java.io.InputStream;

public class Fastjson2Test {

    public static void main(String[] args) throws IOException {
        SkillFire_S2C_Msg skillFire_s2C_msg = DataUtil.build(SkillFire_S2C_Msg.class);
        ClassLoader classLoader = ProtoSerializeBenchMark.class.getClassLoader();
        try (InputStream is = classLoader.getResourceAsStream("along/message.txt")) {
            skillFire_s2C_msg = JSON.parseObject(is, SkillFire_S2C_Msg.class);
        }


        System.out.println(JSON.toJSONString(skillFire_s2C_msg));
        System.out.println(new String(JSON.toJSONBytes(skillFire_s2C_msg)));
        System.out.println(new String(JSONB.toBytes(skillFire_s2C_msg)));

        byte[] bytes = JSONB.toBytes(skillFire_s2C_msg, JSONWriter.Feature.BeanToArray);
        SkillFire_S2C_Msg2 skillFireS2CMsg = JSONB.parseObject(bytes, SkillFire_S2C_Msg2.class, JSONReader.Feature.SupportArrayToBean);
        System.out.println(JSON.toJSONString(skillFireS2CMsg));


    }
}

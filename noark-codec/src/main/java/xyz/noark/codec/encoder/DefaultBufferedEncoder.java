/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 *
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 *
 *        http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package xyz.noark.codec.encoder;

import com.alibaba.fastjson2.util.JDKUtils;
import org.apache.fury.memory.Platform;
import sun.misc.Unsafe;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author 小流氓[176543888@qq.com]
 */
public class DefaultBufferedEncoder extends AbstractBufferedEncoder {
    private static final Unsafe UNSAFE = Platform.UNSAFE;
    //private final OutputStream out;
    private final byte[] buffer;
    private int writeIndex;

    public DefaultBufferedEncoder(byte[] buffer) {
        super(100);
        this.buffer = buffer;
        this.writeIndex = 0;
    }

    @Override
    public void writeInt32(int value) throws IOException {
        //this.flushIfNotAvailable(10);
        bufferFixed32NoTag(value);
    }

    final void bufferFixed32NoTag(long value) throws IOException {
        // -16 到 127
        if (value >= -16 && value <= 127) {
            buffer[writeIndex++] = (byte) value;
        }
        //
        else if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE) {
            buffer[writeIndex++] = (byte) 0x83;
            buffer[writeIndex++] = (byte) value;
        }
        //
        else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
            buffer[writeIndex++] = (byte) 0x84;
            UNSAFE.putShort(buffer, writeIndex, (short) value);
            writeIndex += 2;
        }
        //
        else if (value >= Integer.MIN_VALUE && value <= Integer.MAX_VALUE) {
            buffer[writeIndex++] = (byte) 0x85;
            UNSAFE.putInt(buffer, writeIndex, (int) value);
            writeIndex += 4;
        }
        //
        else {
            buffer[writeIndex++] = (byte) 0x86;
            UNSAFE.putLong(buffer, writeIndex, value);
            writeIndex += 8;
        }
    }

    @Override
    public void writeInt64(long value) throws IOException {
        bufferFixed32NoTag(value);
    }

    static final long STRING_VALUE_FIELD_OFFSET;

    static {
        try {
            // Make offset compatible with graalvm native image.
            STRING_VALUE_FIELD_OFFSET =
                    Platform.objectFieldOffset(String.class.getDeclaredField("value"));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    // Make offset compatible with graalvm native image.
    private static final long STRING_CODER_FIELD_OFFSET;

    static {
        try {
            STRING_CODER_FIELD_OFFSET = Platform.objectFieldOffset(String.class.getDeclaredField("coder"));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void writeString(String value) throws IOException {
        //System.out.println(value);


        byte[] bytes = JDKUtils.STRING_VALUE.apply(value);
        //byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
        this.writeInt32(bytes.length);
        //System.out.println(Arrays.toString(bytes));
        //byte[] bytes = ((byte[]) Platform.getObject(value, STRING_VALUE_FIELD_OFFSET));
        //byte[] bytes = value.getBytes(StandardCharsets.UTF_16);
        //System.out.println(Arrays.toString(bytes));
        System.arraycopy(bytes, 0, buffer, writeIndex, bytes.length);
        writeIndex += bytes.length;
        //buffer[writeIndex++]=
    }

    @Override
    public void writeBytes(byte[] bytes) throws IOException {
        //System.out.println(value);
        this.writeInt32(bytes.length);
        System.arraycopy(bytes, 0, buffer, writeIndex, bytes.length);
        writeIndex += bytes.length;
        //buffer[writeIndex++]=
    }

    @Override
    public void writeBoolean() throws IOException {
        UNSAFE.putByte(buffer, writeIndex++, (byte) 0x81);
    }

    @Override
    public void close() throws IOException {
        this.flush();

    }

    @Override
    public void flush() throws IOException {
        //out.flush();
    }

    public byte[] toByteArray() {
        return Arrays.copyOf(buffer, writeIndex);
    }
}
